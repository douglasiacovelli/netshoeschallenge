package com.iacovelli.netshoeschallenge.home

import com.iacovelli.netshoeschallenge.R
import com.iacovelli.netshoeschallenge.RxImmediateSchedulerRule
import com.iacovelli.netshoeschallenge.models.File
import com.iacovelli.netshoeschallenge.models.Gist
import com.iacovelli.netshoeschallenge.models.User
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Observable
import junit.framework.TestCase.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import java.io.IOException

@RunWith(MockitoJUnitRunner::class)
class HomePresenterTest {
    @Rule @JvmField val rxImmediateScheduler = RxImmediateSchedulerRule()
    @Mock lateinit var contract: HomeContract
    @Mock lateinit var getGists: GetGists

    @Test
    fun testFetchData() {
        val files = hashMapOf<String, File>(Pair("abc", File("abc", null, "rawUrl")))
        val gists = listOf(Gist("url", "abc123", files, User("john", "avatarUrl")))
        Mockito.`when`(getGists.execute(0)).thenReturn(Observable.just(gists))

        val presenter = HomePresenter(contract, getGists)
        assertEquals(1, presenter.localGists?.size)
        assertEquals(1, presenter.nextPage)
        verify(getGists).execute(0)
        verify(contract).setupList(any())
    }

    @Test
    fun testFetchNextPage() {
        val files = hashMapOf<String, File>(Pair("abc", File("abc", null, "rawUrl")))
        val gists = listOf(Gist("url", "abc123", files, User("john", "avatarUrl")))
        Mockito.`when`(getGists.execute(0)).thenReturn(Observable.just(gists))

        val presenter = HomePresenter(contract, getGists)
        verify(getGists).execute(0)

        Mockito.`when`(getGists.execute(1)).thenReturn(Observable.just(gists))
        presenter.fetchNextPage()

        verify(getGists).execute(1)
    }

    @Test
    fun testFetchDataWithError() {
        Mockito.`when`(getGists.execute(0)).thenReturn(Observable.error(IOException("internet connection error")))

        val presenter = HomePresenter(contract, getGists)
        verify(getGists).execute(0)
        verify(contract, never()).setupList(any())
        verify(contract).showMessage(R.string.network_error)
    }
}