package com.iacovelli.netshoeschallenge.common.gistlist

import com.iacovelli.netshoeschallenge.models.File
import com.iacovelli.netshoeschallenge.models.LocalGist
import com.iacovelli.netshoeschallenge.models.User
import junit.framework.Assert
import org.junit.Test

class GistInfoPresenterTest {
    private val GIST_ID = "1234"

    @Test
    fun testShouldNotShowLanguage() {
        val gist = instantiateGist()
        gist.file.language = null

        val presenter = GistInfoPresenter(gist)
        Assert.assertFalse(presenter.languageVisible)
    }

    @Test
    fun testShouldShowLanguage() {
        val gist = instantiateGist()
        gist.file.language = "JSON"
        val presenter = GistInfoPresenter(gist)

        Assert.assertTrue(presenter.languageVisible)
    }

    @Test
    fun testGetFilename() {
        val gist = instantiateGist()
        gist.file.filename = "readme.md"
        val presenter = GistInfoPresenter(gist)

        Assert.assertEquals("readme.md", presenter.filename)
    }

    @Test
    fun testGetLanguageWhenNotNull() {
        val gist = instantiateGist()
        gist.file.language = "JSON"
        val presenter = GistInfoPresenter(gist)

        Assert.assertEquals("JSON", presenter.language)
    }

    @Test
    fun testGetLanguageWhenNull() {
        val gist = instantiateGist()
        gist.file.language = null
        val presenter = GistInfoPresenter(gist)

        Assert.assertEquals("", presenter.language)
    }

    private fun instantiateGist(): LocalGist {
        val user = User("john", "")
        return LocalGist("url", GIST_ID, File("abc", null, "rawUrl"), user)
    }
}
