package com.iacovelli.netshoeschallenge.common

import com.iacovelli.netshoeschallenge.common.gistlist.ConvertListOfGistsToLocalGists
import com.iacovelli.netshoeschallenge.models.File
import com.iacovelli.netshoeschallenge.models.Gist
import com.iacovelli.netshoeschallenge.models.User
import org.junit.Assert
import org.junit.Test

class ConvertListOfGistsToLocalGistTest {
    @Test
    fun execute() {
        val list = listOf(Gist(
                "a",
                "1",
                hashMapOf(Pair("abc", File("abc", null, ""))),
                User("john", "http")
        ))

        ConvertListOfGistsToLocalGists().execute(list).subscribe({
            Assert.assertEquals("abc", it.first().file.filename)
            Assert.assertEquals("john", it.first().owner?.login)
            Assert.assertEquals("a", it.first().url)
            Assert.assertEquals("1", it.first().id)
            Assert.assertNull(it.first().file.language)
        }, {
            Assert.fail()
        })


    }

}