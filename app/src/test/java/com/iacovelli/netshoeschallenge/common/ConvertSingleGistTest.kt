package com.iacovelli.netshoeschallenge.common

import com.iacovelli.netshoeschallenge.common.gistlist.ConvertSingleGist
import com.iacovelli.netshoeschallenge.models.File
import com.iacovelli.netshoeschallenge.models.Gist
import com.iacovelli.netshoeschallenge.models.User
import org.junit.Assert
import org.junit.Test

class ConvertSingleGistTest {
    @Test
    fun execute() {
        val gist = Gist(
                "a",
                "1",
                hashMapOf(Pair("abc", File("abc", null, ""))),
                User("john", "http")
        )

        val cachedGist = ConvertSingleGist().execute(gist)
        Assert.assertEquals("abc", cachedGist.file.filename)
        Assert.assertEquals("john", cachedGist.owner?.login)
        Assert.assertEquals("a", cachedGist.url)
        Assert.assertEquals("1", cachedGist.id)
        Assert.assertNull(cachedGist.file.language)
    }
}