package com.iacovelli.netshoeschallenge.gistdetails

import com.iacovelli.netshoeschallenge.RxImmediateSchedulerRule
import com.iacovelli.netshoeschallenge.common.gistlist.ConvertSingleGist
import com.iacovelli.netshoeschallenge.models.File
import com.iacovelli.netshoeschallenge.models.Gist
import com.iacovelli.netshoeschallenge.models.User
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Observable
import junit.framework.Assert
import junit.framework.Assert.assertFalse
import junit.framework.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GistDetailsPresenterTest {

    @JvmField @Rule val rxSchedulerRules = RxImmediateSchedulerRule()
    @Mock lateinit var contract: GistDetailsContract
    @Mock lateinit var getGist: GetGist
    @Mock lateinit var addToFavorites: AddToFavorites
    @Mock lateinit var removeFromFavorites: RemoveFromFavorites
    @Mock lateinit var isGistInFavorites: IsGistInFavorites
    private val GIST_ID = "1234"

    @Test
    fun testFetchData() {
        val gist = instantiateGist()

        Mockito.`when`(getGist.execute(GIST_ID)).thenReturn(Observable.just(gist))
        val presenter = instantiatePresenter()

        Assert.assertEquals("abc", presenter.gistInfoPresenter.filename)
        verify(contract).updateOptionsMenu()
        verify(contract, never()).showMessage(any())
    }

    @Test
    fun testIsGistInFavorites() {
        val gist = instantiateGist()
        Mockito.`when`(getGist.execute(GIST_ID)).thenReturn(Observable.just(gist))
        Mockito.`when`(isGistInFavorites.execute(GIST_ID)).thenReturn(true)
        val presenter = instantiatePresenter()

        assertTrue(presenter.favorited!!)
    }

    @Test
    fun testAddGistToFavorites() {
        val gist = instantiateGist()
        Mockito.`when`(getGist.execute(GIST_ID)).thenReturn(Observable.just(gist))
        Mockito.`when`(isGistInFavorites.execute(GIST_ID)).thenReturn(false)
        val presenter = instantiatePresenter()

        assertFalse(presenter.favorited!!)
        presenter.addToFavorite()
        verify(addToFavorites).execute(presenter.gistInfoPresenter.gist!!)
        verify(contract, times(2)).updateOptionsMenu()
        assertTrue(presenter.favorited!!)
    }

    @Test
    fun testRemoveGistFromFavorites() {
        val gist = instantiateGist()
        Mockito.`when`(getGist.execute(GIST_ID)).thenReturn(Observable.just(gist))
        Mockito.`when`(isGistInFavorites.execute(GIST_ID)).thenReturn(true)
        val presenter = instantiatePresenter()

        assertTrue(presenter.favorited!!)
        presenter.removeFromFavorite()
        verify(removeFromFavorites).execute(GIST_ID)
        verify(contract, times(2)).updateOptionsMenu()
        assertFalse(presenter.favorited!!)
    }

    private fun instantiatePresenter() =
            GistDetailsPresenter(contract, GIST_ID, getGist, ConvertSingleGist(),
                    addToFavorites, removeFromFavorites, isGistInFavorites)

    private fun instantiateGist(): Gist {
        val fileHashMap = hashMapOf(Pair("abc", File("abc", null, "rawUrl")))
        val user = User("john", "")
        return Gist("url", GIST_ID, fileHashMap, user)
    }

}