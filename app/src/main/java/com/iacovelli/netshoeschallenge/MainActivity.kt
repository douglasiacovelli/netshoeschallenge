package com.iacovelli.netshoeschallenge

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.iacovelli.netshoeschallenge.about.AboutFragment
import com.iacovelli.netshoeschallenge.databinding.ActivityMainBinding
import com.iacovelli.netshoeschallenge.favorites.FavoritesFragment
import com.iacovelli.netshoeschallenge.home.HomeFragment

class MainActivity : AppCompatActivity() {
    lateinit var dataBinding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        inflateFragment(HomeFragment())
        setupBottomNavigation()
    }

    private fun setupBottomNavigation() {
        dataBinding.navigation.setOnNavigationItemSelectedListener({

            val fragment: Fragment = when (it.itemId) {
                R.id.navigation_home -> {
                    HomeFragment()
                }
                R.id.navigation_favorites -> {
                    FavoritesFragment()
                }
                R.id.navigation_about -> {
                    AboutFragment()
                }
                else -> {
                    return@setOnNavigationItemSelectedListener false
                }
            }
            inflateFragment(fragment)

            return@setOnNavigationItemSelectedListener true
        })
    }

    private fun inflateFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.screen_holder, fragment)
                .commit()
    }
}
