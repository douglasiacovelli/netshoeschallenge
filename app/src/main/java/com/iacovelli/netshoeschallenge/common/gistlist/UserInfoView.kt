package com.iacovelli.netshoeschallenge.common.gistlist

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.iacovelli.netshoeschallenge.R
import com.iacovelli.netshoeschallenge.databinding.UserInfoViewBinding
import com.iacovelli.netshoeschallenge.models.User

class UserInfoView(context: Context, attrs: AttributeSet): LinearLayout(context, attrs) {
    private var dataBinding: UserInfoViewBinding? = null

    init {
        if (isInEditMode) {
            inflate(context, R.layout.user_info_view, this)
        } else {
            dataBinding = UserInfoViewBinding.inflate(
                    LayoutInflater.from(context),
                    this,
                    true)
        }
    }

    fun setUser(user: User) {
        Log.d("debug", "setUserCustomView")
        dataBinding?.user = user
        dataBinding?.executePendingBindings()
    }
}