package com.iacovelli.netshoeschallenge.common.gistlist

import com.iacovelli.netshoeschallenge.models.LocalGist
import com.iacovelli.netshoeschallenge.models.Gist

class ConvertSingleGist {
    fun execute(gist: Gist): LocalGist {
        return LocalGist(gist.url, gist.id, gist.files.values.first(), gist.owner)
    }
}