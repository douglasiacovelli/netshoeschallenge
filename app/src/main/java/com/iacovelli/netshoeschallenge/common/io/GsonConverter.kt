package com.iacovelli.netshoeschallenge.common.io

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder

object GsonConverter {

    var instance = GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create()
}