package com.iacovelli.netshoeschallenge.common.picasso

import android.databinding.BindingAdapter
import android.util.Log
import android.widget.ImageView
import com.iacovelli.netshoeschallenge.R
import com.squareup.picasso.Picasso

class PicassoImageBinder {

    companion object {
        @JvmStatic @BindingAdapter("circleImageUrl")
        fun setImageUrl(imageView: ImageView, url: String?) {
            Picasso.with(imageView.context)
                    .load(url)
                    .fit()
                    .centerCrop()
                    .transform(CircleTransformation())
                    .placeholder(R.color.gray)
                    .into(imageView)
        }
    }

}