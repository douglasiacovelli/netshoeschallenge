package com.iacovelli.netshoeschallenge.common.gistlist

import android.databinding.BindingAdapter
import android.util.Log
import com.iacovelli.netshoeschallenge.models.User

class UserInfoBinder {

    companion object {
        @BindingAdapter("user")
        @JvmStatic fun setUser(view: UserInfoView, user: User?) {
            Log.d("debug", "setUserCustomView")
            if (user == null) {
                view.setUser(User("", null))
            } else {
                view.setUser(user)
            }
        }
    }

}