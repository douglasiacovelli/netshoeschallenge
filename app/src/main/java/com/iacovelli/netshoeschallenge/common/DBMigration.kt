package com.iacovelli.netshoeschallenge.common

import io.realm.DynamicRealm
import io.realm.FieldAttribute
import io.realm.RealmMigration

class DBMigration: RealmMigration {

    override fun migrate(realm: DynamicRealm, oldVersion: Long, newVersion: Long) {
        var latestVersion = oldVersion.toInt()
        val schema = realm.schema

        if (latestVersion == 0) {
            schema.create("User")
                    .addField("login", String::class.java)
                    .addField("avatarUrl", String::class.java)

            schema.create("File")
                    .addField("filename", String::class.java)
                    .addField("language", String::class.java)
                    .addField("rawUrl", String::class.java)

            schema.create("LocalGist")
                    .addField("url", String::class.java)
                    .addField("id", String::class.java, FieldAttribute.PRIMARY_KEY)
                    .addRealmObjectField("file", schema.get("File")!!)
                    .addRealmObjectField("owner", schema.get("User")!!)

            latestVersion++
        }

    }
}