package com.iacovelli.netshoeschallenge.common.io.services

import com.iacovelli.netshoeschallenge.models.Gist
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GistService {

    @GET("gists/public?per_page=10")
    fun getGists(@Query("nextPage") page: Int): Observable<List<Gist>>

    @GET("gists/{id}")
    fun getSingleGist(@Path("id") id: String): Observable<Gist>
}