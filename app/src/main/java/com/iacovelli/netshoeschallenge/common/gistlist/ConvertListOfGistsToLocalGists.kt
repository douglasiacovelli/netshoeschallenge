package com.iacovelli.netshoeschallenge.common.gistlist

import com.iacovelli.netshoeschallenge.models.Gist
import com.iacovelli.netshoeschallenge.models.LocalGist
import io.reactivex.Observable

class ConvertListOfGistsToLocalGists {
    fun execute(gists: List<Gist>): Observable<MutableList<LocalGist>> {
        val result = mutableListOf<LocalGist>()
        gists.mapTo(result, transform = {
            ConvertSingleGist().execute(it)
        })
        return Observable.just(result)
    }
}