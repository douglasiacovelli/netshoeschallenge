package com.iacovelli.netshoeschallenge.common.gistlist

import com.iacovelli.netshoeschallenge.models.LocalGist

class ItemGistPresenter(private var contract: GistContract?,
                        private val gist: LocalGist) {

    val gistInfoPresenter = GistInfoPresenter(gist)

    fun onClick() {
        contract?.openGist(gist)
    }

    fun onDestroy() {
        contract = null
    }
}