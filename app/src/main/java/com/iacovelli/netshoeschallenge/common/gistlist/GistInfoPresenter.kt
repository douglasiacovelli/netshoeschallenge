package com.iacovelli.netshoeschallenge.common.gistlist

import android.databinding.BaseObservable
import com.iacovelli.netshoeschallenge.models.LocalGist
import com.iacovelli.netshoeschallenge.models.User

class GistInfoPresenter(var gist: LocalGist? = null): BaseObservable() {

    val filename: String
        get() = gist?.file?.filename ?: ""
    val language: String
        get() = gist?.file?.language ?: ""
    val languageVisible: Boolean
        get() = language.isNotBlank()
    val user: User?
        get() = gist?.owner

}