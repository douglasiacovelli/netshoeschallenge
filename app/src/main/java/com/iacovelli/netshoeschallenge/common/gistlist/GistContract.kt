package com.iacovelli.netshoeschallenge.common.gistlist

import com.iacovelli.netshoeschallenge.models.LocalGist

interface GistContract {
    fun openGist(gist: LocalGist)
    fun setupList(gists: MutableList<LocalGist>)
}