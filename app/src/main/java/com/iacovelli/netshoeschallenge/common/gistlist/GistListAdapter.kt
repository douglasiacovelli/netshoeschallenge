package com.iacovelli.netshoeschallenge.common.gistlist

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.iacovelli.netshoeschallenge.BR
import com.iacovelli.netshoeschallenge.R
import com.iacovelli.netshoeschallenge.models.LocalGist

/**
 * Created by douglasiacovelli on 17/11/17.
 */
class GistListAdapter(private val contract: GistContract,
                      var data: MutableList<LocalGist>) :
        RecyclerView.Adapter<GistListAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val presenter = ItemGistPresenter(contract, data[position])
        holder!!.dataBinding.setVariable(BR.presenter, presenter)
        holder.presenter = presenter
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent!!.context)

        return ViewHolder(
                DataBindingUtil.inflate(inflater, R.layout.item_gist, parent, false))
    }

    override fun getItemCount() = data.size

    override fun onViewRecycled(holder: ViewHolder?) {
        super.onViewRecycled(holder)
        holder?.onViewRecycled()
    }

    class ViewHolder(val dataBinding: ViewDataBinding) : RecyclerView.ViewHolder(dataBinding.root) {
        var presenter: ItemGistPresenter? = null

        fun onViewRecycled() {
            presenter?.onDestroy()
            presenter = null
        }
    }

    fun addNextResults(nextResults: List<LocalGist>) {
        val lastPosition = data.size
        data.addAll(nextResults)
        notifyItemRangeInserted(lastPosition, nextResults.size)
    }

    fun remove(id: String) {
        var index = -1
        val iterator = data.listIterator()
        while (iterator.hasNext()) {
            index++
            val localGist = iterator.next()
            if (localGist.id == id) {
                iterator.remove()
                notifyItemRemoved(index)
            }
        }
    }
}