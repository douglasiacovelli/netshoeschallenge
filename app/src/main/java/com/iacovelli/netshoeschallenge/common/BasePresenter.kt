package com.iacovelli.netshoeschallenge.common

import android.databinding.BaseObservable
import android.databinding.Bindable

abstract class BasePresenter: BaseObservable() {

    @Bindable
    var loading = false
        set(value) {
            field = value
            notifyChange()
        }

}