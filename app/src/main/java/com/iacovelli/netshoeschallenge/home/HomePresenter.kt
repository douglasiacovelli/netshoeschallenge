package com.iacovelli.netshoeschallenge.home

import com.iacovelli.netshoeschallenge.R
import com.iacovelli.netshoeschallenge.common.BasePresenter
import com.iacovelli.netshoeschallenge.common.gistlist.ConvertListOfGistsToLocalGists
import com.iacovelli.netshoeschallenge.models.LocalGist
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.io.IOException

class HomePresenter(private val contract: HomeContract,
                    private val getGists: GetGists): BasePresenter() {
    private var compositeDisposable: CompositeDisposable = CompositeDisposable()
    var nextPage = 0
        private set
    var localGists: MutableList<LocalGist>? = null
        private set

    init {
        fetchData()
    }

    fun onDestroy() {
        compositeDisposable.clear()
    }

    private fun fetchData() {
        loading = true
        val disposable = getGists.execute(nextPage)
                .subscribeOn(Schedulers.io())
                .flatMap {
                    ConvertListOfGistsToLocalGists().execute(it)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnTerminate({
                    loading = false
                })
                .subscribe({
                    localGists = it
                    contract.setupList(it)
                    nextPage++
                }, {
                    if (it is IOException) {
                        contract.showMessage(R.string.network_error)
                    } else {
                        contract.showMessage(R.string.unexpected)
                    }
                })

        compositeDisposable.add(disposable)
    }

    fun fetchNextPage(): Observable<List<LocalGist>> {
        return getGists.execute(nextPage)
                .flatMap {
                    ConvertListOfGistsToLocalGists().execute(it)
                }
    }
}