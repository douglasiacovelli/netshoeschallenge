package com.iacovelli.netshoeschallenge.home

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.iacovelli.netshoeschallenge.R
import com.iacovelli.netshoeschallenge.common.gistlist.GistListAdapter
import com.iacovelli.netshoeschallenge.databinding.GistListBinding
import com.iacovelli.netshoeschallenge.gistdetails.GistDetailsActivity
import com.iacovelli.netshoeschallenge.models.LocalGist
import com.jakewharton.rxbinding2.support.v7.widget.RxRecyclerView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class HomeFragment : Fragment(), HomeContract {

    private lateinit var dataBinding: GistListBinding
    private var presenter: HomePresenter? = null
    private val compositeDisposable = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.gist_list, container, false)
        presenter = HomePresenter(this, GetGists())
        dataBinding.presenter = presenter

        return dataBinding.root
    }

    override fun setupList(gists: MutableList<LocalGist>) {
        val recyclerView = dataBinding.list
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.adapter = GistListAdapter(this, gists)

        setupPaginatorListener()
    }

    override fun onStop() {
        super.onStop()
        presenter?.onDestroy()
        compositeDisposable.clear()
    }

    override fun openGist(gist: LocalGist) {
        startActivity(GistDetailsActivity.buildIntent(activity!!, gist.id))
    }

    override fun showMessage(resId: Int) {
        Toast.makeText(activity, resId, Toast.LENGTH_LONG).show()
    }

    override fun onResume() {
        super.onResume()
        setupPaginatorListener()
    }

    private fun setupPaginatorListener() {
        if (compositeDisposable.size() > 0 || dataBinding.list.adapter == null) return
        val disposable = RxRecyclerView.scrollEvents(dataBinding.list)
                .flatMap {
                    Observable.just((dataBinding.list.layoutManager as LinearLayoutManager)
                            .findLastVisibleItemPosition())
                }
                .filter {
                    // Fetches a new nextPage if there's only 4 elements left
                    it + 4 >= dataBinding.list.adapter.itemCount && presenter != null
                }
                .observeOn(Schedulers.io())
                .flatMap {
                    presenter!!.fetchNextPage()
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    (dataBinding.list.adapter as GistListAdapter).addNextResults(it)
                }, {
                    Log.e("debug", "error while fetching next nextPage")
                })

        compositeDisposable.add(disposable)

    }

}
