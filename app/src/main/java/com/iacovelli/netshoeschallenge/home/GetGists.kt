package com.iacovelli.netshoeschallenge.home

import com.iacovelli.netshoeschallenge.common.io.NetshoesRetrofit
import com.iacovelli.netshoeschallenge.models.Gist
import com.iacovelli.netshoeschallenge.common.io.services.GistService
import io.reactivex.Observable

class GetGists {
    fun execute(page: Int): Observable<List<Gist>> {
        return NetshoesRetrofit.instance.create(GistService::class.java)
                .getGists(page)
    }
}