package com.iacovelli.netshoeschallenge.home

import android.support.annotation.StringRes
import com.iacovelli.netshoeschallenge.common.gistlist.GistContract

interface HomeContract: GistContract {
    fun showMessage(@StringRes resId: Int)
}