package com.iacovelli.netshoeschallenge.models

import io.realm.RealmObject

open class User (var login: String, var avatarUrl: String?): RealmObject() {
    constructor(): this("", null)
}