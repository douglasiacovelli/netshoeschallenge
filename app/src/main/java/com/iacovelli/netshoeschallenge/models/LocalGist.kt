package com.iacovelli.netshoeschallenge.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class LocalGist(var url: String,
                     @PrimaryKey var id: String,
                     var file: File,
                     var owner: User?): RealmObject() {

    constructor(): this("", "", File(), User())
}