package com.iacovelli.netshoeschallenge.models

import io.realm.RealmObject

open class File (var filename: String, var language: String?, var rawUrl: String): RealmObject() {
    constructor(): this("", null, "")
}