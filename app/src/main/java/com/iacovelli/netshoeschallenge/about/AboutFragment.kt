package com.iacovelli.netshoeschallenge.about

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.iacovelli.netshoeschallenge.R
import com.iacovelli.netshoeschallenge.databinding.FragmentAboutBinding

class AboutFragment : Fragment() {

    private lateinit var dataBinding: FragmentAboutBinding
    var step = 0
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_about, container, false)

        setupOnLogoutButtonClick()
        return dataBinding.root
    }

    private fun setupOnLogoutButtonClick() {
        dataBinding.logoutButton.setOnClickListener {
            onClickLogout()
        }
    }

    private fun onClickLogout() {
        val message = when (step) {
            0 -> R.string.logout_message_0
            1 -> R.string.logout_message_1
            2 -> R.string.logout_message_2
            3 -> R.string.logout_message_3
            4 -> R.string.logout_message_4
            else -> 0
        }

        if (message != 0) {
            Toast.makeText(activity, message, Toast.LENGTH_LONG).show()
            step++
        } else {
            activity?.finish()
        }
    }

}
