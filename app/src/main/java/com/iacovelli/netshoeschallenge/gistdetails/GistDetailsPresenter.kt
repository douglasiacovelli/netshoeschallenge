package com.iacovelli.netshoeschallenge.gistdetails

import android.databinding.Bindable
import com.iacovelli.netshoeschallenge.R
import com.iacovelli.netshoeschallenge.common.BasePresenter
import com.iacovelli.netshoeschallenge.common.gistlist.ConvertSingleGist
import com.iacovelli.netshoeschallenge.common.gistlist.GistInfoPresenter
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.io.IOException

class GistDetailsPresenter(private val contract: GistDetailsContract,
                           private val gistId: String,
                           private val getGist: GetGist,
                           private val convertSingleGist: ConvertSingleGist,
                           private val addToFavorites: AddToFavorites,
                           private val removeFromFavorites: RemoveFromFavorites,
                           private val isGistInFavorites: IsGistInFavorites): BasePresenter() {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    @Bindable
    var gistInfoPresenter = GistInfoPresenter()
    var favorited: Boolean? = null

    init {
        fetchData()
    }

    fun onDestroy() {
        compositeDisposable.clear()
    }

    fun onClickSeeGist() {
        contract.openGistFile(gistInfoPresenter.gist!!.file.rawUrl)
    }

    fun addToFavorite() {
        favorited = favorited!!.not()
        addToFavorites.execute(gistInfoPresenter.gist!!)
        contract.showMessage(R.string.added_to_favorites)
        contract.updateOptionsMenu()
    }

    fun removeFromFavorite() {
        favorited = favorited!!.not()
        removeFromFavorites.execute(gistInfoPresenter.gist!!.id)
        contract.showMessage(R.string.removed_from_favorites)
        contract.updateOptionsMenu()
    }

    fun getGistId() =
            gistInfoPresenter.gist?.id

    private fun fetchData() {
        loading = true
        favorited = isGistInFavorites.execute(gistId)

        val disposable = getGist.execute(gistId)
                .subscribeOn(Schedulers.io())
                .flatMap {
                    Observable.just(convertSingleGist.execute(it))
                }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnTerminate({
                    loading = false
                })
                .subscribe({
                    gistInfoPresenter.gist = it
                    contract.updateOptionsMenu()
                    notifyChange()
                }, {
                    loading = false
                    if (it is IOException) {
                        contract.showMessage(R.string.network_error)
                    } else {
                        contract.showMessage(R.string.unexpected)
                    }
                })

        compositeDisposable.add(disposable)
    }
}
