package com.iacovelli.netshoeschallenge.gistdetails

import com.iacovelli.netshoeschallenge.models.LocalGist
import io.realm.Realm

class AddToFavorites {
    fun execute(gist: LocalGist) {
        val realm = Realm.getDefaultInstance()
        realm.executeTransaction({
            it.copyToRealmOrUpdate(gist)
        })
        realm.close()
    }
}