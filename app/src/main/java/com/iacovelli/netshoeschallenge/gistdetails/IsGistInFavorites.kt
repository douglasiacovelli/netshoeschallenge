package com.iacovelli.netshoeschallenge.gistdetails

import com.iacovelli.netshoeschallenge.models.LocalGist
import io.realm.Realm

class IsGistInFavorites {
    fun execute(id: String): Boolean {
        var response = false
        val realm = Realm.getDefaultInstance()
        val gist = realm.where(LocalGist::class.java).equalTo("id", id).findFirst()
        response = gist != null
        realm.close()
        return response
    }
}