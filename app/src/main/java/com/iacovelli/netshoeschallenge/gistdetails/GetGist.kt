package com.iacovelli.netshoeschallenge.gistdetails

import com.iacovelli.netshoeschallenge.common.io.NetshoesRetrofit
import com.iacovelli.netshoeschallenge.common.io.services.GistService
import com.iacovelli.netshoeschallenge.models.Gist
import io.reactivex.Observable

class GetGist {
    fun execute(id: String): Observable<Gist> {
        return NetshoesRetrofit.instance.create(GistService::class.java)
                .getSingleGist(id)
    }
}