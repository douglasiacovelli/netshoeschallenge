package com.iacovelli.netshoeschallenge.gistdetails

import com.iacovelli.netshoeschallenge.models.LocalGist
import io.realm.Realm

class RemoveFromFavorites {
    fun execute(id: String) {
        val realm = Realm.getDefaultInstance()
        val gist = realm.where(LocalGist::class.java).equalTo("id", id).findFirst()
        gist?.let {
            realm.executeTransaction({
                gist.deleteFromRealm()
            })
        }
        realm.close()
    }
}