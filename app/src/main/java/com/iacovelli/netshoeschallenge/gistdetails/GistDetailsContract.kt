package com.iacovelli.netshoeschallenge.gistdetails

import android.support.annotation.StringRes

interface GistDetailsContract {
    fun showMessage(@StringRes resId: Int)
    fun openGistFile(url: String)
    fun updateOptionsMenu()
}
