package com.iacovelli.netshoeschallenge.gistdetails

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.Bundle
import android.support.customtabs.CustomTabsIntent
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.iacovelli.netshoeschallenge.R
import com.iacovelli.netshoeschallenge.common.gistlist.ConvertSingleGist
import com.iacovelli.netshoeschallenge.databinding.ActivityGistDetailsBinding
import com.iacovelli.netshoeschallenge.favorites.FavoritesFragment.Companion.RESULT_REMOVED

class GistDetailsActivity : AppCompatActivity(), GistDetailsContract {
    lateinit var dataBinding: ActivityGistDetailsBinding
    var presenter: GistDetailsPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val gistId = intent!!.extras.getString(GIST_ID)
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_gist_details)
        presenter = GistDetailsPresenter(this, gistId, GetGist(), ConvertSingleGist(),
                AddToFavorites(), RemoveFromFavorites(), IsGistInFavorites())

        dataBinding.presenter = presenter
        setupToolbar()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.onDestroy()
    }

    override fun showMessage(resId: Int) {
        Toast.makeText(this, resId, Toast.LENGTH_LONG).show()
    }

    override fun openGistFile(url: String) {
        CustomTabsIntent.Builder()
                .setToolbarColor(ContextCompat.getColor(this,R.color.colorPrimary))
                .build()
                .launchUrl(this, Uri.parse(url))
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when(item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                false
            }
            R.id.add -> {
                presenter?.addToFavorite()
                false
            }
            R.id.remove -> {
                presenter?.removeFromFavorite()
                false
            }
            else -> {
                return super.onOptionsItemSelected(item)
            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (presenter?.favorited != null) {
            menuInflater.inflate(R.menu.favorite, menu)

            if (presenter!!.favorited!!) {
                showAddButton(menu)
            } else {
                showRemoveButton(menu)
            }
            return true
        }
        return super.onCreateOptionsMenu(menu)
    }

    private fun setupToolbar() {
        setSupportActionBar(dataBinding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onBackPressed() {
        if (presenter?.favorited == false) {
            val intent = Intent()
            intent.putExtra(GIST_ID, presenter?.getGistId())
            setResult(RESULT_REMOVED, intent)
        } else {
            setResult(Activity.RESULT_CANCELED)
        }
        finish()
    }

    override fun updateOptionsMenu() {
        invalidateOptionsMenu()
    }

    private fun showRemoveButton(menu: Menu?) {
        menu!!.findItem(R.id.remove).isVisible = false
        menu.findItem(R.id.add).isVisible = true
    }

    private fun showAddButton(menu: Menu?) {
        menu!!.findItem(R.id.add).isVisible = false
        menu.findItem(R.id.remove).isVisible = true
    }

    companion object {
        @JvmField
        val GIST_ID = "com.iacovelli.netshoeschallenge.GIST_ID"

        @JvmStatic
        fun buildIntent(context: Context, id: String): Intent {
            val intent = Intent(context, GistDetailsActivity::class.java)
            intent.putExtra(GIST_ID, id)
            return intent
        }
    }
}
