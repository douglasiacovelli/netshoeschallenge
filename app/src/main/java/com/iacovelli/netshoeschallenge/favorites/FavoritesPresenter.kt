package com.iacovelli.netshoeschallenge.favorites

import com.iacovelli.netshoeschallenge.common.BasePresenter
import com.iacovelli.netshoeschallenge.models.LocalGist

class FavoritesPresenter(private val contract: FavoritesContract,
                         private val getFavoriteGists: GetFavoriteGists) : BasePresenter() {
    private var favoriteGists: MutableList<LocalGist>? = null

    init {
        fetchData()
    }

    private fun fetchData() {
        favoriteGists = getFavoriteGists.execute()
        contract.setupList(favoriteGists!!)
    }

}