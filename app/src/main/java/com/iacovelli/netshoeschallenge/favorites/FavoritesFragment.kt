package com.iacovelli.netshoeschallenge.favorites

import android.app.Activity.RESULT_CANCELED
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.iacovelli.netshoeschallenge.R
import com.iacovelli.netshoeschallenge.common.gistlist.GistListAdapter
import com.iacovelli.netshoeschallenge.databinding.GistListBinding
import com.iacovelli.netshoeschallenge.gistdetails.GistDetailsActivity
import com.iacovelli.netshoeschallenge.gistdetails.GistDetailsActivity.Companion.GIST_ID
import com.iacovelli.netshoeschallenge.models.LocalGist

class FavoritesFragment : Fragment(), FavoritesContract {

    private lateinit var dataBinding: GistListBinding
    private var presenter: FavoritesPresenter? = null
    private val REQUEST_GIST_DETAIL = 100

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.gist_list, container, false)
        presenter = FavoritesPresenter(this, GetFavoriteGists())
        dataBinding.presenter = presenter

        return dataBinding.root
    }

    override fun setupList(gists: MutableList<LocalGist>) {
        val recyclerView = dataBinding.list
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.adapter = GistListAdapter(this, gists)
    }

    override fun onStop() {
        super.onStop()
    }

    override fun openGist(gist: LocalGist) {
        startActivityForResult(GistDetailsActivity.buildIntent(activity!!, gist.id), REQUEST_GIST_DETAIL)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_GIST_DETAIL && resultCode != RESULT_CANCELED) {
            if (resultCode == RESULT_REMOVED && dataBinding.list.adapter != null) {
                (dataBinding.list.adapter as GistListAdapter).remove(data!!.extras.getString(GIST_ID))
            }
        }
    }

    companion object {
        @JvmField val RESULT_REMOVED = 2
    }

}
