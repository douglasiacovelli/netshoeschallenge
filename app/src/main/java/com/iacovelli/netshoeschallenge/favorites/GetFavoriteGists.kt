package com.iacovelli.netshoeschallenge.favorites

import com.iacovelli.netshoeschallenge.models.LocalGist
import io.realm.Realm

class GetFavoriteGists {
    fun execute(): MutableList<LocalGist> {
        val realm = Realm.getDefaultInstance()
        val realmGists = realm.where(LocalGist::class.java).findAll()

        val result: MutableList<LocalGist> = if (realmGists.isEmpty()) {
            mutableListOf()
        } else {
            realm.copyFromRealm(realmGists)
        }
        realm.close()
        return result
    }
}